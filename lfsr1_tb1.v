
`define WIDTH 16
module tb();
 reg clk;
 reg reset;
 reg [`WIDTH - 1 : 0]data;

 wire [`WIDTH - 1 : 0]out1;
 wire [`WIDTH - 1 : 0]out2;
 wire [`WIDTH - 1 : 0]out3;
 wire [`WIDTH - 1 : 0]out4;
 wire [`WIDTH - 1 : 0]out5;

initial begin
  $monitor("reset %b clock %b data %b out1 %b out2 %b out3 %b out4 %b out5 %b",
		reset, clk, data, out1, out2, out3, out4, out5);
  clk = 0;
  reset = 1;
  data = 16'b1101011100101011;	
  #10 reset = 0;
  #100 $finish;
end 

always #5 clk = !clk;

lfsr1 LFSR1 (
 .clk	( clk ),
 .reset ( reset ),
 .data  ( data  ),
 .out1   ( out1   ),
 .out2   ( out2   ),
 .out3   ( out3   ),
 .out4   ( out4   ),
 .out5   ( out5   )
);

endmodule
