module top_end (
		input a,
		input b,
//		input c,
//		input d,
		output e
//		output f
	);

// assign a = 1'b1;
assign b = 1'b1;
/*
assign _and[1].bot_and1.a = 1'b1;
assign _and[1].bot_and1.b = 1'b1;
*/
genvar i;
generate
for (i = 0; i < 2; i = i + 1)
	begin: _and
wire a1;

assign a1 = (i == 0) ? 1 : 0;
AND bot_and1 (
	.a	(a1),
	.b	(b),
	.c	(e)
	);
end
endgenerate

/*
AND bot_and2 (
	.a	(c),
	.b	(d),
	.c	(f)
	);
*/
//AND _and[0].bot_and1.a = 1'b1;
//AND _and[0].bot_and1.b = 1'b1;
/*
assign a = 2'b1;
assign b = 2'b10;
assign c = 2'b0;
assign d = 2'b11;
AND bot_and1 (.a (a), .b (b), .c (e));
AND bot_and2 (.a (c), .b (d), .c (f));
// AND _and[1].bot_and1 (c, d, f);
*/
endmodule
