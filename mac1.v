////////////////////////////
// Module: MAC
// Function: Perform Multiply and Add
//		of the inputs and direct
//		the result in output
// Author: Mayank Parasar
// Contact: mparasar3@gatech.edu
//////////////////////////////////

module mac1 #(parameter LEN = 16 )(
	input clk,
	input reset,
	input [LEN-1 : 0] din_m_0,	
	input [LEN-1 : 0] din_m_1, 	
	input [LEN-1 : 0] din_a,	
	output reg [LEN-1 : 0] dout
	);
// Temporary variable to save the results
// of intermidiate calculation
wire [2*LEN - 1 : 0] prod;
wire [2*LEN : 0] sum;

	assign prod = din_m_0 * din_m_1;
	assign sum = prod + din_a;

// Put the result thus computed in 'dout' 	
// Need to lower down the bit-width to match
// 'dout'

lfsr2 LFSR2 ( .clk (clk), .reset (reset), .data ( dout ), .out1 (), .out2 (), .out3 (), .out4 ());

always @(posedge clk) begin
dout <= (sum > ((1 << LEN)-1)) ? ((1 << LEN) - 1) : sum [LEN - 1 : 0];
$display("dout: %b", dout);
end


/*
always @(*) begin
$display("dout: %b", dout);
end 
*/
endmodule
