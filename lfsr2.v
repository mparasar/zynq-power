
`define WIDTH 16

module lfsr2 (
clk	, // Clock input
reset	, // Reset input
data	, // Input
out1	, // Output-1
out2	, // Output-2
out3	, // Output-3
out4	 // Output-4
);

// Instantiating (embedding) MAC module inside lfsr module (named association)
// #1
mac2 MAC1 ( .clk	(clk), .din_m_0	( out1 ),  .din_m_1	( out1 ),  .din_a	( out1 ), .dout ( ) );

// #2
mac2 MAC2 ( .clk	(clk), .din_m_0	( out2 ), .din_m_1	( out2 ), .din_a	( out2 ), .dout ( ));

// #3
mac2 MAC3 ( .clk	(clk), .din_m_0	( out3 ), .din_m_1	( out3 ), .din_a	( out3 ), .dout ( ));

// #4
mac2 MAC4 ( .clk	(clk), .din_m_0	( out4 ), .din_m_1	( out4 ), .din_a	( out4 ), .dout ( ));

input clk;
input reset;
input [`WIDTH - 1 : 0]data;

output reg [`WIDTH - 1 : 0]out1;
output reg [`WIDTH - 1 : 0]out2;
output reg [`WIDTH - 1 : 0]out3;
output reg [`WIDTH - 1 : 0]out4;
	 
always @(posedge clk) begin

 if (reset) begin
	out1 <= data;
	out2 <= data;
	out3 <= data;
	out4 <= data;
	end
 else begin
	out1 <= {/*(^(out1 & `WIDTH'b011011)),*/ out1[`WIDTH-2:0], (^(out1 & `WIDTH'b011011))};
	out2 <= {/*(^(out2 & `WIDTH'b011100)),*/ out2[`WIDTH-2:0], ~(^(out2 & `WIDTH'b011100))};
	out3 <= {/*(^(out3 & `WIDTH'b011110)),*/ out3[`WIDTH-2:0], (^(out3 & `WIDTH'b001110))};
	out4 <= {/*(^(out4 & `WIDTH'b000101)),*/ out4[`WIDTH-2:0], ~(^(out4 & `WIDTH'b100101))};
	end
end
endmodule
