`define WIDTH 16

module top (
clk	, // Clock input
reset	, // Reset input
data	, // Data Output
);

input clk;
input reset;
output reg [`WIDTH - 1 : 0]data;

genvar i;
generate
for (i = 0; i < 10; i = i + 1)
	begin: _top
lfsr1 LFSR1 (.clk (clk), .reset (reset), .data (data)); 
end
endgenerate

endmodule
