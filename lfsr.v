////////////////////////////
// Module: LFSR_UPDOWN
// Function: generate random input number
//		underlying principal is
//		LFSR (Linear Feedback Shift Register)
// Author: Mayank Parasar
// Contact: mparasar3@gatech.edu
//////////////////////////////////

`define WIDTH 64 
module lfsr (
clk       ,   // Clock input
reset     ,   // Reset input
enable    ,   // Enable input
seed1	,
seed2	,
seed3	,
count1    ,   // Count output1
count2	  ,   // Count output2		
count3	  ,   // Count output3	
);

// Instantiating MAC module inside lfsr module (named association)
mac MAC(
  .din_m_0	( din_m_0 ),
  .din_m_1	( din_m_1 ),
  .din_a	( din_a ),
  .dout		( dout )
);


// We are not fanning-out here!
/*genvar i;
generate for(i = 0; i < 10; i = i + 1) begin : macc_instance
	macc instance_macc(
	  .din_m_0	( din_m_0 ),
	  .din_m_1	( din_m_1 ),
	  .din_a	( din_a ),
	  .dout		( dout )
	);
end 
endgenerate
*/

 input clk;
 input reset;
 input enable; 
 input [`WIDTH-1 : 0] seed1;
 input [`WIDTH-1 : 0] seed2;
 input [`WIDTH-1 : 0] seed3;


 output [`WIDTH-1 : 0] count1;
 output [`WIDTH-1 : 0] count2;
 output [`WIDTH-1 : 0] count3;

 reg [`WIDTH-1 : 0] count1;
 reg [`WIDTH-1 : 0] count2;
 reg [`WIDTH-1 : 0] count3;
 

 reg [`WIDTH-1 : 0] din_m_0;
 reg [`WIDTH-1 : 0] din_m_1;
 reg [`WIDTH-1 : 0] din_a;
 wire[`WIDTH-1 : 0] dout;


always @(posedge clk) begin

 if (reset) begin 
    count1 <= seed1;
    count2 <= seed2;
    count3 <= seed3;
	end
/*
 else if (enable) begin
//    if (up_down) begin
      count1 <= {~(^(count1 & `WIDTH'b01100011)),count1[`WIDTH-1:1]};
      count2 <= {~(^(count2 & `WIDTH'b01100011)),count2[`WIDTH-1:1]};
      count3 <= {~(^(count3 & `WIDTH'b01100011)),count3[`WIDTH-1:1]};
//	/*instance_macc.macc_instance[1].din_m_0 <= count1;
//	/*instance_macc.macc_instance[1].din_m_1 <= count2;
//	/*instance_macc.macc_instance[1].din_a <= count3;	
	$display ("dout: %b", dout);
//    end else begin
//      count1 <= {count1[`WIDTH-2:0],~(^(count1 &  `WIDTH'b10110001))};
//      count2 <= {count2[`WIDTH-2:0],~(^(count2 &  `WIDTH'b10110001))};
//      count3 <= {count3[`WIDTH-2:0],~(^(count3 &  `WIDTH'b10110001))};
//	macc_instance[1].din_m_0 <= count1;
//	macc_instance[1].din_m_1 <= count2;
//	macc_instance[1].din_a <= count3;
//	$display ("dout: %b", dout);
   end
*/
 end


endmodule

