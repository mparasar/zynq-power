////////////////////////////
// Module: MAC
// Function: Perform Multiply and Add
//		of the inputs and direct
//		the result in output
// Author: Mayank Parasar
// Contact: mparasar3@gatech.edu
//////////////////////////////////

module mac #(parameter LEN = 16 )(
	input [LEN-1 : 0] din_m_0,	
	input [LEN-1 : 0] din_m_1, 	
	input [LEN-1 : 0] din_a,	
	output [LEN-1 : 0] dout
	);
// Temporary variable to save the results
// of intermidiate calculation
wire [2*LEN - 1 : 0] prod;
wire [2*LEN : 0] sum;

	assign prod = din_m_0 * din_m_1;
	assign sum = prod + din_a;

// Put the result thus computed in 'dout' 	
// Need to lower down the bit-width to match
// 'dout'

assign dout = (sum > ((1 << LEN)-1)) ? ((1 << LEN) - 1) : sum [LEN - 1 : 0];

endmodule
