////////////////////////////
// Module: Top Module
// Function: 	This will call both module
//           	MAC and LFSR; Intend is to have
//	     	many instances of MAC driven by
//	     	LFSR; Thus this module would 
//		drive the inputs for both module
//		and would check the output.
//		TestBench is needed to check the 
//		correctness of this module
//		This module would facilitate the 
//		functioning between other module
//		to create random traffic in output	
// Author: Mayank Parasar
// Contact: mparasar3@gatech.edu
//////////////////////////////////

`define WIDTH 64 
module top_module (
	input clk       ,   // Clock input: LFSR
	input reset     ,   // Reset input: LFSR
	input enable    ,   // Enable input: LFSR
	output [`WIDTH - 1 : 0]seed1   ,   // Count output1: LFSR
	output [`WIDTH - 1 : 0]seed2   ,   // Count output2: LFSR
	output [`WIDTH - 1 : 0]seed3	   // Count output3: LFSR
);

reg [`WIDTH - 1 : 0]seed1;
reg [`WIDTH - 1 : 0]seed2;
reg [`WIDTH - 1 : 0]seed3;

wire [`WIDTH - 1 : 0]cnt1;
wire [`WIDTH - 1 : 0]cnt2;
wire [`WIDTH - 1 : 0]cnt3;

genvar i;
generate
	for (i = 0; i < 4; i = i + 1)
	begin: inst_lfsr // Generated names will have inst_lfsr[i].prefix
	lfsr lfsr_inst(		// Named Association (Instantiation of each LFSR-module)		
				.clk      ( clk      ),
				.reset    ( reset    ),
				.enable   ( enable   ),
				.seed1    ( seed1   ),
				.seed2    ( seed2   ),
				.seed3    ( seed3   ),
				.count1   ( cnt1   ),
				.count2   ( cnt2   ),
				.count3   ( cnt3   )
				);
	end	
endgenerate
//integer j;
always @(posedge clk) begin
if (reset) begin 
//	for (j = 0; j < 10; j = j + 1)	begin
	// Initializing each lfsr module's initial value
		// #1
		lfsr inst_lfsr[0].lfsr_inst(clk, reset, enable, {`WIDTH{1'b0}}, {{`WIDTH-4{1'b0}}, 4'b10}, {{`WIDTH-2{1'b0}}, 2'b01}, cnt1, cnt2, cnt3);
//  	 	 inst_lfsr[0].lfsr_inst.seed1 <= {`WIDTH{1'b0}};
//    		 inst_lfsr[0].lfsr_inst.seed2 <= {{`WIDTH-4{1'b0}}, 4'b10};
//    		 inst_lfsr[0].lfsr_inst.seed3 <= {{`WIDTH-2{1'b0}}, 2'b01};
		// #2
  	 	 inst_lfsr[1].lfsr_inst.seed1 <= {`WIDTH{1'b1}};
    		 inst_lfsr[1].lfsr_inst.seed2 <= {{`WIDTH-5{1'b0}}, 5'b11};
    		 inst_lfsr[1].lfsr_inst.seed3 <= {{`WIDTH-3{1'b0}}, 3'b10};
		// #3
  	 	 inst_lfsr[2].lfsr_inst.seed1 <= {`WIDTH{2'b10}};
    		 inst_lfsr[2].lfsr_inst.seed2 <= {{`WIDTH-6{1'b0}}, 6'b100};
    		 inst_lfsr[2].lfsr_inst.seed3 <= {{`WIDTH-4{1'b0}}, 4'b11};
		// #4
  	 	 inst_lfsr[3].lfsr_inst.seed1 <= {`WIDTH{2'b11}};
    		 inst_lfsr[3].lfsr_inst.seed2 <= {{`WIDTH-3{1'b0}}, 3'b101};
    		 inst_lfsr[3].lfsr_inst.seed3 <= {{`WIDTH-3{1'b0}}, 3'b100};
		// #5
  	 	 inst_lfsr[4].lfsr_inst.seed1 <= {`WIDTH{3'b100}};
    		 inst_lfsr[4].lfsr_inst.seed2 <= {{`WIDTH-3{1'b0}}, 3'b110};
    		 inst_lfsr[4].lfsr_inst.seed3 <= {{`WIDTH-3{1'b0}}, 3'b101};
		// #6
  	 	 inst_lfsr[5].lfsr_inst.seed1 <= {`WIDTH{3'b101}};
    		 inst_lfsr[5].lfsr_inst.seed2 <= {{`WIDTH-3{1'b0}}, 3'b111};
    		 inst_lfsr[5].lfsr_inst.seed3 <= {{`WIDTH-3{1'b0}}, 3'b110};
		// #7
  	 	 inst_lfsr[6].lfsr_inst.seed1 <= {`WIDTH{3'b110}};
    		 inst_lfsr[6].lfsr_inst.seed2 <= {{`WIDTH-4{1'b0}}, 4'b1000};
    		 inst_lfsr[6].lfsr_inst.seed3 <= {{`WIDTH-3{1'b0}}, 3'b111};
		// #8
  	 	 inst_lfsr[7].lfsr_inst.seed1 <= {`WIDTH{3'b111}};
    		 inst_lfsr[7].lfsr_inst.seed2 <= {{`WIDTH-4{1'b0}}, 4'b1001};
    		 inst_lfsr[7].lfsr_inst.seed3 <= {{`WIDTH-4{1'b0}}, 4'b1000};
		// #9
  	 	 inst_lfsr[8].lfsr_inst.seed1 <= {`WIDTH{4'b1000}};
    		 inst_lfsr[8].lfsr_inst.seed2 <= {{`WIDTH-4{1'b0}}, 4'b1010};
    		 inst_lfsr[8].lfsr_inst.seed3 <= {{`WIDTH-4{1'b0}}, 4'b1001};
		// #10
  	 	 inst_lfsr[9].lfsr_inst.seed1 <= {`WIDTH{4'b1001}};
    		 inst_lfsr[9].lfsr_inst.seed2 <= {{`WIDTH-4{1'b0}}, 4'b1011};
    		 inst_lfsr[9].lfsr_inst.seed3 <= {{`WIDTH-4{1'b0}}, 4'b1010};
//		end
	end
 else if (enable) begin
/*
//    if (up_down) begin
	// #1
      inst_lfsr[0].lfsr_inst.seed1 <= {~(^(inst_lfsr[0].lfsr_inst.count1 & `WIDTH'b01100011)),inst_lfsr[0].lfsr_inst.count1[`WIDTH-1:1]};
      inst_lfsr[0].lfsr_inst.seed2 <= {~(^(inst_lfsr[0].lfsr_inst.count2 & `WIDTH'b01100011)),inst_lfsr[0].lfsr_inst.count2[`WIDTH-1:1]};
      inst_lfsr[0].lfsr_inst.seed3 <= {~(^(inst_lfsr[0].lfsr_inst.count3 & `WIDTH'b01100011)),inst_lfsr[0].lfsr_inst.count3[`WIDTH-1:1]};
//      inst_mac[0].mac_inst(inst_lfsr[0].lfsr_inst.count1, inst_lfsr[0].lfsr_inst.count2, inst_lfsr[0].lfsr_inst.count3, inst_mac[0].mac_inst.dout);

// 	assign inst_mac[0].mac_inst.din_m_0 <= inst_lfsr[0].lfsr_inst.count1;
//     	assign inst_mac[0].mac_inst.din_m_1 <= inst_lfsr[0].lfsr_inst.count2;
//     	assign inst_mac[0].mac_inst.din_a <= inst_lfsr[0].lfsr_inst.count3;
//	/*instance_macc.macc_instance[1].din_m_1 <= count2;
//	/*instance_macc.macc_instance[1].din_a <= count3;	

*/
	end
end // always-end
	
endmodule
