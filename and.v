module AND (
	input a,
	input b,
	output reg c
	);

// assign c = a & b;
initial begin
 assign c = (a & b);
 $display("a: %b, b: %b, c: %b \n", a, b, c);
end
endmodule
