
`define WIDTH 16
module mac1_tb1();
reg clk;
reg reset;
reg [`WIDTH - 1 : 0]data1;
reg [`WIDTH - 1 : 0]data2;
reg [`WIDTH - 1 : 0]data3;
wire [`WIDTH - 1 : 0]out;

initial begin
  $monitor("reset %b clock %b data1 %b data2 %b data3 %b out %b",
		reset, clk, data1, data2, data3, out);
  clk = 0;
  reset = 1;
  data1 = 16'b011;	
  data2 = 16'b011;	
  data3 = 16'b011;	
//  out = 16'b011;	
  #10 reset = 0;
  #100 $finish;
end 

always #5 clk = !clk;

mac1 MA1 (
 .clk	( clk ),
 .reset	( reset),
 .din_m_0 ( data1 ),
 .din_m_1 ( data2 ),
 .din_a	(data3),
 .dout (out)
);
endmodule
